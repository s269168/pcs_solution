#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x,
             const double &y)
{   _x = x;
    _y = y;
}

Ellipse::Ellipse(const Point &center,
                 const int &a,
                 const int &b)
{
    _center = Point(center);
    _a = a;
    _b = b;

}

double Ellipse::Area() const
{
    return M_PI *_a*_b;
}

Circle::Circle(const Point &center,
               const int &radius)
{
    _center = Point(center);
    _radius = radius;
}

double Circle::Area() const
{
    return M_PI* _radius * _radius;
}

Triangle::Triangle(const Point &p1,
                   const Point &p2,
                   const Point &p3)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p3 = Point(p3);
}

double Triangle::Area() const
{
    double area;
    area = abs((_p1._x-_p2._x)*(_p3._y-_p2._y)-((_p3._x-_p2._x)*((_p1._y-_p2._y))));
    return area/2;

}
TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    _p1 = Point(p1);
    _edge = edge;
}

double TriangleEquilateral::Area() const
{
    return (sqrt(3)/4)*_edge * _edge;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p3 = Point(p3);
    _p4 = Point(p4);
}

double Quadrilateral::Area() const
{
    Triangle triangle1 = Triangle(_p1, _p2, _p4);
    Triangle triangle2 = Triangle(_p2, _p3, _p4);

    double areaTriangle1 = triangle1.Area();
    double areaTriangle2 = triangle2.Area();

    return areaTriangle1 + areaTriangle2;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p4 = Point(p4);
}

double Parallelogram::Area() const
{
    Triangle triangleP= Triangle(_p1,_p2,_p4);
    double areaParallelogram = (triangleP.Area())*2;
    return areaParallelogram;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    _p1 = Point(p1);
    _base = base;
    _height = height;
}

double Rectangle::Area() const
{
    return _base*_height;
}

Square::Square(const Point &p1, const int &edge)
{
    _p1 = Point(p1);
    _edge = edge;
}

double Square::Area() const
{
    return _edge*_edge;
}

}
