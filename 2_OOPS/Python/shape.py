import math as math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self) -> float:
        return math.pi * self._a * self._b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        self.__center = center
        self.__radius = radius

    def area(self) -> float:
        return math.pi * self.__radius * self.__radius



class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def area(self) -> float:
        area = abs((self._p1.x-self._p2.x)*(self._p3.y-self._p2.y)-((self._p3.x-self._p2.x)*((self._p1.y-self._p2.y))))/2
        return area



class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self.__p1= p1
        self.__edge = edge

    def area(self) -> float:
        return (math.sqrt(3)/4)*self.__edge * self.__edge


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self) -> float:
        triangle1 = Triangle(self._p1, self._p2, self._p4)
        triangle2 = Triangle(self._p2, self._p3, self._p4)

        area1 = triangle1.area()
        area2 = triangle2.area()

        return area1 + area2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p4 = p4

    def area(self) -> float:
        triangle = Triangle(self._p1, self._p2, self._p4)
        area = triangle.area()

        return area*2


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self._p1 = p1
        self._base = base
        self._height = height

    def area(self) -> float:
        return self._base* self._height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        self.__p1 = p1
        self.__edge = edge

    def area(self) -> float:
        return self.__edge * self.__edge
