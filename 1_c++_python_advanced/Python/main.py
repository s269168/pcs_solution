import numpy as np
import scipy.linalg as sp


# \brief Test the real solution of system Ax = b
# \return the relative error for PALU solver
# \return the relative error for QR solver
def testSolution(A, b, solution):
    solutionPALU = solveSystemPALU(A,b)
    solutionQR = solveSystemQR(A,b)

    errRelPALU = np.linalg.norm(solutionPALU-solution)/np.linalg.norm(solution)
    errRelQR = np.linalg.norm(solutionQR-solution)/np.linalg.norm(solution)

    return errRelPALU, errRelQR


# \brief Solve linear system with PALU
# \return the solution
def solveSystemPALU(A, b):
    lu,piv = sp.lu_factor(A)
    x = sp.lu_solve((lu, piv), b)
    return x


# \brief Solve linear system with PALU
# \return the solution
def solveSystemQR(A, b):
    Q, R = sp.qr(A)
    y = np.dot(Q.T,b)
    x = sp.solve(R,y)
    return x



if __name__ == '__main__':
    solution = [-1.0, -1.0]

    A1 = [[5.547001962252291e-01, -3.770900990025203e-02],[ 8.320502943378437e-01, -9.992887623566787e-01]]
    b1 = [-5.169911863249772e-01, 1.672384680188350e-01]
    [errRel1PALU, errRel1QR] = testSolution(A1, b1, solution)
    if errRel1PALU < 1e-15 and errRel1QR < 1e-15:
        print("1 - PALU: {:.4e}, QR: {:.4e}".format(errRel1PALU, errRel1QR))
    else:
        print("1 - Wrong system solution found")
        exit(-1)

    A2 = [[5.547001962252291e-01, -5.540607316466765e-01],[8.320502943378437e-01, -8.324762492991313e-01]]
    b2 =  [-6.394645785530173e-04, 4.259549612877223e-04]
    [errRel2PALU, errRel2QR] = testSolution(A2, b2, solution)
    if errRel2PALU < 1e-12 and errRel2QR < 1e-12:
        print("2 - PALU: {:.4e}, QR: {:.4e}".format(errRel2PALU, errRel2QR))
    else:
        print("2 - Wrong system solution found")
        exit(-1)

    A3 = [[5.547001962252291e-01, -5.547001955851905e-01],[ 8.320502943378437e-01, -8.320502947645361e-01]]
    b3 = [-6.400391328043042e-10, 4.266924591433963e-10]
    [errRel3PALU, errRel3QR] = testSolution(A3, b3, solution)
    if errRel3PALU < 1e-5 and errRel3QR < 1e-5:
        print("3 - PALU: {:.4e}, QR: {:.4e}".format(errRel3PALU, errRel3QR))
    else:
        print("3 - Wrong system solution found")
        exit(-1)

    exit(0)
