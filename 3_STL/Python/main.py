class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredient=[]

    def addIngredient(self, ingredient: Ingredient):
        if ingredient in self.ingredient:
            ingredient.Description = "Reinforcement"
        self.ingredient.append(ingredient)

    def numIngredients(self) -> int:
        return(len(self.ingredient))

    def computePrice(self) -> int:
       price = 0
       for ingredient in self.ingredient:
           price = price + ingredient.Price
       return price

class Order:
    def __init__(self):
        self.pizza=[]
        self.numOrder : int = 0

    def getPizza(self, position: int) -> Pizza:
        if position < len(self.pizza):
            return self.pizza[position -1]
        else:
            raise ValueError("Position passed is wrong")

    def initializeOrder(self, numPizzas: int):
        self.numOrder = numPizzas + 1000

    def addPizza(self, pizza: Pizza):
        self.pizza.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizza)

    def computeTotal(self) -> int:
        price = 0
        for i in range(0, len(self.pizza)):
            price = price + self.pizza[i].computePrice()
        return price

class Pizzeria:
    def __init__(self):
        self.pizza = []
        self.ingredient = []
        self.order = []


    def addIngredient(self, name: str, description: str, price: int):
        for i in range(0, len(self.ingredient)):
            if name in self.ingredient[i].Name:
                raise ValueError("Ingredient already inserted")
        self.ingredient.append(Ingredient(name, price, description))

    def findIngredient(self, name: str) -> Ingredient:
        for i in self.ingredient:
            if name in i.Name:
                return i
        else:
            raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for i in (0, len(self.pizza)):
            if name in self.pizza[i].Name:
                raise ValueError("Pizza already inserted")
        self.pizza.append(Pizza(name).addIngredient(self.findIngredient(ingredients)))


    def findPizza(self, name: str) -> Pizza:
        for i in self.pizza:
            if name in self.pizza:
                return i
        raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise ValueError("Empty order")
        order = Order()
        order.initializeOrder(len(pizzas))
        for i in range(0,len(pizzas)):
           order.addPizza(self.findPizza(pizzas[i]))
        self.order.append(order)
        return order.numOrder

    def findOrder(self, numOrder: int) -> Order:
        if numOrder < len(self.order) + 1000:
            return self.order[numOrder-1000]
        else:
            raise ValueError("Position Passed is wrong")


    def getReceipt(self, numOrder: int) -> str:
        numberReceipt = self.findOrder(numOrder)
        receipt = ""
        for pizza in numberReceipt.pizza:
            receipt = receipt+ "- " + str(pizza.Name) + ", " + str(pizza.computePrice()) + " euro" + "\n"
        receipt = receipt + "  TOTAL: " + str(numberReceipt.computeTotal()) + " euro" + "\n"
        return receipt


    def listIngredients(self) -> str:
        ingredient = ""
        self.ingredient.sort(key=lambda x: x.Name)
        for ingredient in self.ingredient:
            ingredient = ingredient + str(ingredient.Name) + " - '" + str(ingredient.Description) + "': " + str(ingredient.Price) + " euro" + "\n"
        return ingredient


def menu(self) -> str:
    menu = ""
    for pizza in self.pizzamenu:
        menu = menu + str(pizza.Name) + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) + " euro" + "\n"
    return menu
