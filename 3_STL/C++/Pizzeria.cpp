#include "Pizzeria.h"
#include<string>

namespace PizzeriaLibrary {

int Pizza::ComputePrice() const
{
    int price = 0;
    unsigned int numIngredients = this ->NumIngredients();

    for (unsigned int i=0; i < numIngredients; i++){
        price += _ingredient[i].Price;
    }
    return price;
}

void Order::InitializeOrder(int numPizzas)
{
    if (!numPizzas)
        throw runtime_error("Empty Order");
}

const Pizza &Order::GetPizza(const int &position) const
{
    if (position <= _pizza.size())
        return _pizza[position-1];
    else
        throw  runtime_error("Position passed is wrong");
}

int Order::ComputeTotal() const
{
    int price = 0;
    unsigned int numPizzas = _pizza.size();

    for (unsigned int i = 0; i < numPizzas; i++){
        price += _pizza[i].ComputePrice();
    }
    return price;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    for (list<Ingredient>::iterator it = _kitchenIngredients.begin(); it != _kitchenIngredients.end(); it++){
        if (it -> Name == name){
            throw runtime_error("Pizza already inserted");
        }
       }

    _kitchenIngredients.push_back(Ingredient(name, description, price));
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    for (list<Ingredient>::iterator it = _kitchenIngredients.begin(); it != _kitchenIngredients.end(); it++){
       if (it->Name == name){
           return *it;
        }
    }
    throw  runtime_error("Ingredient not found");
}
void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    for (vector<Pizza>::iterator it = _pizzaList.begin(); it != _pizzaList.end(); it++){
        if (it ->Name == name)
            throw runtime_error("Pizza already inserted");
    }
      _pizzaList.push_back(Pizza(name));
      for (unsigned int i=0; i< ingredients.size(); i++){
      Pizza & pizza = _pizzaList.back();
      pizza.AddIngredient(FindIngredient(ingredients[i]));
      }
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    for (vector<Pizza>::iterator it = _pizzaList.begin(); it !=_pizzaList.end(); it++){
        if (it -> Name == name)
            return *it;
    }
    throw  runtime_error("Pizza not Found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    if (!pizzas.size())
        throw runtime_error("Empty order");
    _order.push_back(Order());
    for(unsigned int i = 0; i< pizzas.size(); i++){
        _order.back().AddPizza(this->FindPizza(pizzas[i]));
    }
    return _order.size();  
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
   int numberOrder = _order.size();
   if (numOrder < numberOrder + 1000)
       return _order[numOrder -1000];
   else
       throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    Order numberReceips = this ->FindOrder(numOrder);
    string receipt = "";
    for (unsigned int i = 0; i <numberReceips._pizza.size(); i++){
        receipt = receipt + "- " + numberReceips._pizza[i].Name + ", " + to_string(numberReceips._pizza[i].ComputePrice()) +" euro"+"\n";
    }
    receipt = receipt+ " Total: "+ to_string(numberReceips.ComputeTotal()) + " euro" +"\n";
}

string Pizzeria::ListIngredients() const
{
  string listIngredients = "";
  _kitchenIngredients.sort();
  for (list<Ingredient>::iterator it = _kitchenIngredients.begin(); it != _kitchenIngredients.end(); it++){
      listIngredients = listIngredients + it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro"+"\n";
  }
  return listIngredients;
}

string Pizzeria::Menu() const
{
    string menu= "";
    for(vector<Pizza>::iterator it= _pizzaList.begin(); it != _pizzaList.end(); it++){
        menu = menu + it->Name + " (" + to_string(it->NumIngredients()) + " ingredients): " + to_string(it->ComputePrice()) + " euro"+ "\n";
    }
    return menu;
}
}
