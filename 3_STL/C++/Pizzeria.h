#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

      Ingredient(const string& name, const string& description, const int& price){
          Name= name;
          Price = price;
          Description = description;
      }
     bool operator < (const Ingredient &ingredient){
         return Name < ingredient.Name;
      }
  };

  class Pizza {
    public:
      string Name;
      mutable vector<Ingredient> _ingredient;

      Pizza (const string& name)
      {
          Name = name;
      }

      void AddIngredient(const Ingredient& ingredient) { return _ingredient.push_back(ingredient);}
      int NumIngredients() const { return _ingredient.size();}
      int ComputePrice() const;
  };

  class Order {
    public:
      mutable vector<Pizza> _pizza;
      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza) { _pizza.push_back(pizza);}
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const {return _pizza.size();}
      int ComputeTotal() const;
  };

  class Pizzeria {
      mutable list<Ingredient> _kitchenIngredients;
      mutable vector<Pizza> _pizzaList;
      mutable vector<Order> _order;
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
