#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
    private:
    const string& _busFilePath;
    int _numberOfBuses;
    vector <Bus> _bus;
  public:
    BusStation(const string& busFilePath): _busFilePath(busFilePath){ };
    void Load();
    int NumberBuses() const { return _numberOfBuses; }
    const Bus& GetBus(const int& idBus) const;
};

  class MapData : public IMapData {
  private:
      string _mapFilePath;
      int _numberOfRoutes;
      int _numberOfStreets;
      int _numberBusStops;
      vector <BusStop> _busStops;
      vector <Street> _streets;
      vector <Route> _routes;
      vector <int> _streetFrom;
      vector <int> _streetTo;
      vector<vector<int>> _routeStreets;
    public:
      MapData(const string& mapFilePath): _mapFilePath(mapFilePath){ };
      void Load();
      int NumberRoutes() const { return _numberOfRoutes; }
      int NumberStreets() const { return _numberOfStreets; }
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;
    private:
      const IMapData& _mapData;
      const IBusStation& _busStation;

   public:
    RoutePlanner(const IMapData& mapData,
                 const IBusStation& busStation): _mapData(mapData), _busStation(busStation){ };

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
  private:
      const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData): _mapData(mapData) { };
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
