# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    _bus.clear();
    _numberOfBuses=0;

    ifstream file;
    file.open(_busFilePath.c_str());
    if (file.fail()){
        throw runtime_error("Something goes wrong");
    }

    try{
       istringstream convertLine, busConverter;

       string line;

       getline(file, line);
       getline(file,line);
       convertLine.str(line);
       convertLine>>_numberOfBuses;
       _bus.resize(_numberOfBuses);

       getline(file,line);
       for (int i=0; i<_numberOfBuses; i++){
           getline(file, line);
           busConverter.str(line);
           busConverter>>_bus[i].Id>>_bus[i].FuelCost;
       }
       file.close();
    }
    catch(exception){
        _numberOfBuses=0;
        _bus.clear();
        throw runtime_error("Somenthing goes wrong)");
    }


}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus<= _numberOfBuses)
        return _bus[idBus-1];
    else
        throw runtime_error("Bus "+ to_string(idBus)+" not exist");

}

void MapData::Load()
{
    ifstream file;
    istringstream busConverter, coordinateConverter, numberOfStreetConverter, streetsConverter, numberOfRoutesConverter, routeConverter;
    file.open(_mapFilePath.c_str());
    if(file.fail())
        throw runtime_error("Something goes wrong");

    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetFrom.clear();
    _streetTo.clear();
    _routeStreets.clear();

    _numberOfRoutes = 0;
    _numberOfStreets = 0;
    _numberBusStops = 0;

try{
        string line;
        getline(file, line);
        busConverter.str(line);
        busConverter>> _numberBusStops;
        _busStops.resize(_numberBusStops);

        getline(file,line);
        for (int i=0; i<_numberBusStops; i++){
            getline(file,line);
            coordinateConverter.str(line);
            coordinateConverter>> _busStops[i].Id>>_busStops[i].Name>> _busStops[i].Latitude >> _busStops[i].Longitude;
        }

        getline(file, line);
        getline(file, line);
        numberOfStreetConverter.str(line);
        numberOfStreetConverter>> _numberOfStreets;
        _streetFrom.resize(_numberOfStreets);
        _streetTo.resize(_numberOfStreets);
        _streets.resize(_numberOfStreets);

        getline(file, line);
        for (int i=0; i<_numberOfStreets; i++){
            getline(file,line);
            streetsConverter.str(line);
            streetsConverter>> _streets[i].Id>>_streetFrom[i]>>_streetTo[i] >>_streets[i].TravelTime;
        }

        getline(file,line);
        getline(file,line);
        numberOfRoutesConverter.str(line);
        numberOfRoutesConverter>> _numberOfRoutes;
        _routeStreets.resize(_numberOfRoutes);
        _routes.resize(_numberOfRoutes);

        getline(file,line);

        for( int i=0; i<_numberOfRoutes; i++){
            getline(file, line);
            routeConverter.str(line);
            routeConverter>>_routes[i].Id >> _routes[i].NumberStreets;
            _routeStreets[i].resize(_routes[i].NumberStreets);

            for (int j=0; j<_routes[i].NumberStreets; j++){
                routeConverter>> _routeStreets[i][j];
            }
        }

        file.close();

}  catch (exception) {
        _busStops.clear();
        _streets.clear();
        _routes.clear();
        _streetFrom.clear();
        _streetTo.clear();
        _routeStreets.clear();

        _numberOfRoutes = 0;
        _numberOfStreets = 0;
        _numberBusStops = 0;

        throw runtime_error("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if (idRoute <= _numberOfRoutes){
         const vector<int>& street = _routeStreets[idRoute -1];

    if (streetPosition <= (int)street.size()){
         const int& idStreet = street[streetPosition];
         return GetStreet(idStreet);
    }

    else
        throw runtime_error("Something goes wrong");
    }
    else {
        throw runtime_error("Something goes wrong");
    }
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if (idRoute<= _numberOfRoutes)
        return _routes[idRoute-1];
    else
        throw runtime_error("Route "+ to_string(idRoute)+ " does not exist");
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if (idStreet<= _numberOfStreets)
        return _streets[idStreet-1];
    else
        throw runtime_error("Street "+ to_string(idStreet)+ " does not exist");
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if(idStreet<= _numberOfStreets){
        const int& busStop = _streetFrom[idStreet-1];
        return GetBusStop(busStop);
    }
    else
        throw runtime_error("Street from "+ to_string(idStreet)+ " does not exist");
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if(idStreet<= _numberOfStreets){
            const int& busStop = _streetTo[idStreet-1];
            return GetBusStop(busStop);
        }
      else
        throw runtime_error("Street to "+ to_string(idStreet)+ " does not exist");
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if(idBusStop<= _numberBusStops){
            return _busStops[idBusStop-1];
        }
      else
        throw runtime_error("Bus stop "+ to_string(idBusStop)+ " does not exist");
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    int travelTime=0;
    Route route=_mapData.GetRoute(idRoute);
    Street street;
    int numberOfStreet= route.NumberStreets;

    for (int i=0; i<numberOfStreet; i++){
      street= _mapData.GetRouteStreet(idRoute, i);
      travelTime = travelTime + street.TravelTime;
    }
    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    Bus bus= _busStation.GetBus(idBus);

    int travelTime=0;
    Route route=_mapData.GetRoute(idRoute);
    Street street;
    int numberOfStreet= route.NumberStreets;

    for (int i=0; i<numberOfStreet; i++){
      street= _mapData.GetRouteStreet(idRoute, i);
      travelTime = travelTime + street.TravelTime;
    }
    int totalCost = double(travelTime)/3600 *bus.FuelCost*BusAverageSpeed;

    return totalCost;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
     Route route = _mapData.GetRoute(idRoute);
     Street street;
     BusStop start, arrival;

    ostringstream routeView;
    routeView << route.Id << ": ";
    for (int i=0; i < route.NumberStreets; i++){
       street = _mapData.GetRouteStreet(idRoute, i);
       start= _mapData.GetStreetFrom(street.Id);
        routeView << start.Name << " -> ";
        if (i==route.NumberStreets -1){
            arrival = _mapData.GetStreetTo(street.Id);
            routeView << arrival.Name;
        }
    }

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    BusStop start = _mapData.GetStreetFrom(idStreet);
    BusStop arrival =_mapData.GetStreetTo(idStreet);
    return to_string(idStreet) + + ": " + start.Name + " -> " + arrival.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude/10000) + ", " + to_string((double)busStop.Longitude/10000)+ ")";
}

//
}
