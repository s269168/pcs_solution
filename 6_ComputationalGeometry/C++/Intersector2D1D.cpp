#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{


}
Intersector2D1D::~Intersector2D1D()
{
    delete(planeTranslationPointer);
    delete(planeNormalPointer);
    delete(lineOriginPointer);
    delete(lineTangentPointer);

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  double *translation = new double;
  Vector3d *newVector = new Vector3d;

  *translation= planeTranslation;
  planeTranslationPointer= translation;
  *newVector =planeNormal/planeNormal.norm();
  planeNormalPointer= newVector;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    Vector3d *origin = new Vector3d;
    Vector3d *tangent = new Vector3d;

    *origin = lineOrigin;
    lineOriginPointer = origin;
    *tangent= lineTangent/lineTangent.norm();
    lineTangentPointer= tangent;

}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  Vector3d tangent = *lineTangentPointer;
  Vector3d origin = *lineOriginPointer;
  Vector3d normal = *planeNormalPointer;
  double translation = *planeTranslationPointer;

  if(normal.dot(tangent)> toleranceIntersection){
      intersectionParametricCoordinate = (translation - normal.dot(origin))/normal.dot(tangent);
      intersectionType = PointIntersection;
      return true;
  }
  else{
    if ((normal.dot(origin)- translation)> toleranceParallelism)
        intersectionType = NoInteresection;
    else
        intersectionType = Coplanar;
    return false;

}
}
