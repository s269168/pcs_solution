#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  matrixNomalVector.col(0)= planeNormal;
  rightHandSide(0)= planeTranslation;

}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.col(1)= planeNormal;
    rightHandSide(1)= planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    Vector3d N1 = matrixNomalVector.col(0);
    Vector3d N2 = matrixNomalVector.col(1);
    Vector3d N3 = N1.cross(N2);

    if (N3.norm() > toleranceIntersection){
        intersectionType = LineIntersection;
        tangentLine = N3/N3.norm();
        matrixNomalVector.col(2)= N3;
        rightHandSide(2)=0;
        Vector3d solution= matrixNomalVector.fullPivLu().solve(rightHandSide);
        pointLine = solution;
        return true;
    }
    else{
        if (abs(rightHandSide(0)-rightHandSide(1))<=toleranceParallelism)
            intersectionType = Coplanar;
        else
            intersectionType = NoInteresection;
        return false;

    }

}
