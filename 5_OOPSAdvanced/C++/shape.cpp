#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return (_a+_b)*M_PI;
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    for (unsigned int i=0; i< 3; i++){
         perimeter +=(points[(i+1)%3] - points[i]).ComputeNorm2();
    }

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
      _edge = edge;
      points.push_back(p1);

  }

  double TriangleEquilateral::Perimeter() const
  {
      return _edge*3;

  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for( unsigned int i =0; i< 4; i++){
        perimeter +=(points[(i+1)%4] - points[i]).ComputeNorm2();
    }

    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
    _base = base;
    _height = height;
    points.push_back(p1);
  }

  double Rectangle::Perimeter() const
  {
      return(_base + _height)*2;
  }

  Point Point::operator+(const Point& point) const
  {
      Point tmp = Point(this->X + point.X, this->Y + point.Y);
      return tmp;

  }

  Point Point::operator-(const Point& point) const
  {
      Point tmp = Point(this->X - point.X, this->Y - point.Y);
      return tmp;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;

  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;

  }

}
